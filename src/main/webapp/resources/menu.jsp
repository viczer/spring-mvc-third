<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

<div id="menu">
    <p><a href="<c:url value="/showAll"/>">List Persons</a></p>
    <p><a href="<c:url value="/addPerson"/>">Créer une nouvelle Person</a></p>
    
    <c:set var="numPage" value="1" />
    <c:url value="//showAllByPage/${numPage}/2" var = "monlien"/>
<a href="${monlien}"> Par page </a>

<c:out value = "${numPage}"/>
<%--      <p><a href="<c:url value="/showAllByPage/1/2"/>">Visualizer 2 person pour page</a></p> --%>
</div>